let screen = document.getElementById("screen");
let screenValue = "";
let temp;
buttons = document.querySelectorAll("button");

for (item of buttons) {
  item.addEventListener("click", (e) => {
    buttonText = e.target.innerText;
    if (buttonText == "C") {
      screenValue = " ";
      screen.value = screenValue;
    } else if (buttonText == "×") {
      buttonText = "*";
      screenValue += buttonText;
      screen.value = screenValue;
    } else if (buttonText == "÷") {
      buttonText = "/";
      screenValue += buttonText;
      screen.value = screenValue;
    } else if (buttonText == "⌫") {
      screenValue = screen.value.substr(0, screen.value.length - 1);
      screen.value = screenValue;
    } else if (buttonText == "x2") {
      screenValue = screen.value * screen.value;
      screen.value = screenValue;
    } else if (buttonText == "1/x") {
      screenValue = 1 / screen.value;
      screen.value = screenValue;
    } else if (buttonText == "√x") {
      screenValue = Math.sqrt(screen.value);
      screen.value = screenValue;
    } else if (buttonText == "∛x") {
      screenValue = Math.cbrt(screen.value);
      screen.value = screenValue;
    } else if (buttonText == "| x |") {
      screenValue = Math.abs(screen.value);
      screen.value = screenValue;
    } else if (buttonText == "xy") {
      buttonText = "**";
      screenValue += buttonText;
      screen.value = screenValue;
    } else if (buttonText == "兀") {
      buttonText = "3.14";
      screenValue += buttonText;
      screen.value = screenValue;
    } else if (buttonText == "𝑒") {
      buttonText = "2.718";
      screenValue += buttonText;
      screen.value = screenValue;
    } else if (buttonText == "10x") {
      screenValue = Math.pow(10, screen.value);
      screen.value = screenValue;
    } else if (buttonText == "mod") {
      buttonText = "%";
      screenValue += buttonText;
      screen.value = screenValue;
    } else if (
      buttonText == "Trigonometry" ||
      buttonText == "expand_more" ||
      buttonText == "Functions" ||
      buttonText == " expand_more"
    ) {
      screen.value = "";
    } else if (buttonText == "n!") {
      n = screenValue;
      x = 1;
      for (i = 2; i <= n; i++) x = x * i;
      screenValue = x;
      screen.value = screenValue;
    } else if (buttonText == "exp") {
      screenValue = Math.exp(screenValue).toPrecision(6);
      screen.value = screenValue;
    } else if (buttonText == "log") {
      screenValue = Math.log10(screen.value);
      screen.value = screenValue;
    } else if (buttonText == "ln") {
      screenValue = Math.log(screen.value);
      screen.value = screenValue;
    } else if (buttonText == "DEG") {
      screenValue = (180 * screenValue) / Math.PI;
      screen.value = screenValue.toPrecision(6);
    } else if (buttonText == "RAD") {
      screenValue = (Math.PI * screenValue) / 180;
      screen.value = screenValue.toPrecision(6);
    } else if (buttonText == "=") {
      screenValue = eval(screenValue);
      screen.value = screenValue;
    } else if (buttonText == "MS") {
      temp = screenValue;
      // console.log(temp);
    } else if (buttonText == "M+") {
      temp = temp + parseInt(screenValue);
    } else if (buttonText == "M-") {
      temp = temp - parseInt(screenValue);
    } else if (buttonText == "MC") {
      temp = "";
      screen.value = temp;
    } else if (buttonText == "MR") {
      // console.log(temp);
      screenValue = temp;
      screen.value = screenValue;
    } else {
      screenValue += buttonText;
      screen.value = screenValue;
    }
  });
}

function trigno(tf) {
  let screenValue = screen.innerText;
  if (tf == "sine") {
    screenValue = Math.sin(screen.value);
    screen.value = screenValue;
  }
  if (tf == "cos") {
    screenValue = Math.cos(screen.value);
    screen.value = screenValue;
  }
  if (tf == "tan") {
    screenValue = Math.tan(screen.value);
    screen.value = screenValue;
  }
  if (tf == "cot") {
    screenValue = 1 / Math.tan(screen.value);
    screen.value = screenValue;
  }
  if (tf == "sec") {
    screenValue = 1 / Math.cos(screen.value);
    screen.value = screenValue;
  }
  if (tf == "cosec") {
    screenValue = 1 / Math.sin(screen.value);
    screen.value = screenValue;
  }
}

function func(get) {
  let screenValue = screen.innerText;
  if (get == "ceil") {
    screenValue = Math.ceil(screen.value);
    screen.value = screenValue;
  }
  if (get == "floor") {
    screenValue = Math.floor(screen.value);
    screen.value = screenValue;
  }
  if (get == "round") {
    screenValue = Math.round(screen.value);
    screen.value = screenValue;
  }
}
